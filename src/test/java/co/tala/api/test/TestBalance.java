/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.test;

import co.tala.api.entity.Account;
import co.tala.api.repository.AccountRepository;
import co.tala.api.resource.AccountResource;
import co.tala.api.service.AccountService;
import co.tala.api.wrapper.ResponseWrapper;
import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 *
 * @author aristarchus
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountResource.class)
public class TestBalance {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    String correctAccountNumber = "123456789";
    String wrongAccountNumber = "XXXXXXXX";
    Account correctAccount;
    ResponseWrapper<Object> wrap;
    BigDecimal balance = new BigDecimal("100");

    @Before
    public void SetUp() {
        correctAccount = new Account("Test1", "Test2", correctAccountNumber);
        correctAccount.setBalance(balance);
        wrap = new ResponseWrapper<>();
    }

    @Test
    public void getAccountShouldReturnAccount() throws Exception {
        Mockito.when(accountRepository.findByAccNumber(correctAccountNumber))
                .thenReturn(correctAccount);
        Account found = accountRepository.findByAccNumber(correctAccountNumber);
        assertThat(found.getAccNumber())
                .isEqualTo(correctAccountNumber);
    }

    @Test(expected = NullPointerException.class)
    public void getWrongAccountShouldReturnNull() throws Exception {
        Mockito.when(accountRepository.findByAccNumber(correctAccountNumber))
                .thenReturn(correctAccount);
        Account found = accountRepository.findByAccNumber(wrongAccountNumber);
        assertThat(found.getAccNumber())
                .isEqualTo(correctAccountNumber);
    }

    @Test
    public void processBalanceShouldReturnAccountDetails() throws Exception {
        wrap.setData(correctAccount);
        given(this.accountService.processBalance(correctAccountNumber))
                .willReturn(wrap);
        mockMvc.perform(get("/balance/" + correctAccountNumber)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.balance").value(balance));
    }

    @Test
    public void processInvalidBalanceShouldReturnNull() throws Exception {
        wrap.setData(correctAccount);
        given(this.accountService.processBalance(correctAccountNumber))
                .willReturn(wrap);
        mockMvc.perform(get("/balance/" + wrongAccountNumber)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }
}
