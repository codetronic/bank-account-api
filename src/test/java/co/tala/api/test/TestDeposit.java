/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.test;

import co.tala.api.custom.AppConstants;
import co.tala.api.entity.Account;
import co.tala.api.entity.Transaction;
import co.tala.api.repository.AccountRepository;
import co.tala.api.repository.TransactionRepository;
import co.tala.api.resource.AccountResource;
import co.tala.api.service.AccountService;
import co.tala.api.wrapper.ResponseWrapper;
import co.tala.api.wrapper.TransactionWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 *
 * @author aristarchus
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountResource.class)
public class TestDeposit {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private TransactionRepository transactionRepository;

    String correctAccountNumber = "123456789";
    String wrongAccountNumber = "XXXXXXXX";
    Account correctAccount;
    ResponseWrapper<Object> wrap;
    BigDecimal balance = new BigDecimal("100");
    BigDecimal depositAmount = new BigDecimal("500");
    BigDecimal updatedBalance;
    TransactionWrapper transactionWrapper;
    String requestData;
    List<Transaction> trxList;
    Transaction trx;

    public TestDeposit() {
        this.trxList = new ArrayList<>();
        correctAccount = new Account("Test1", "Test2", correctAccountNumber);
        wrap = new ResponseWrapper<>();
        transactionWrapper = new TransactionWrapper(correctAccountNumber, depositAmount, "Test");
        trx = new Transaction(correctAccount.getId(), AppConstants.TRX_CREDIT, balance);

    }

    @Before
    public void SetUp() throws Exception {
        correctAccount.setBalance(balance);
        correctAccount.setId(1l);
        requestData = mapper.writeValueAsString(transactionWrapper);
        updatedBalance = balance.add(depositAmount);
        trxList.add(trx);
    }

    @Test
    public void processDepositShouldReturnUpdatedAccountDetails() throws Exception {
        Mockito.when(accountRepository.findByAccNumber(correctAccountNumber))
                .thenReturn(correctAccount);
        Mockito.when(transactionRepository.findAllCreatedToday(AppConstants.TRX_CREDIT,
                correctAccount.getId(), new Date(), new Date()))
                .thenReturn(trxList);
        //updated balance
        Account updatedAccount = correctAccount;
        updatedAccount.setBalance(updatedBalance);
        wrap.setData(updatedAccount);

        when(accountService.processDeposit(any(TransactionWrapper.class)))
                .thenReturn(wrap);

        mockMvc.perform(post("/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestData))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.balance").value(updatedBalance));
    }
}
