/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.service;

import co.tala.api.custom.ApiException;
import co.tala.api.custom.AppConstants;
import co.tala.api.entity.Account;
import co.tala.api.entity.Transaction;
import co.tala.api.repository.AccountRepository;
import co.tala.api.repository.TransactionRepository;
import co.tala.api.resource.AccountResource;
import co.tala.api.wrapper.ResponseWrapper;
import co.tala.api.wrapper.TransactionWrapper;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author aristarchus
 */
@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    /**
     * retrieveBalance
     *
     * @param accountNumber
     * @return
     */
    public ResponseWrapper processBalance(String accountNumber) {
        ResponseWrapper wrap = new ResponseWrapper();
        try {
            Account account = this.getAccount(accountNumber);
            if (account == null) {
                throw new ApiException(AppConstants.INVALID_ACC_NO);
            }
            wrap = this.okWrapper(account);
        } catch (ApiException e) {
            wrap = this.errWrapper(e.getMessage());
        }
        return wrap;
    }

    /**
     * processDeposit
     *
     * @param transactionWrapper
     * @return
     */
    public synchronized ResponseWrapper processDeposit(TransactionWrapper transactionWrapper) {
        ResponseWrapper wrap = new ResponseWrapper();
        try {
            //Amounts must be greater than 0
            if (transactionWrapper.getAmount().compareTo(new BigDecimal(1)) < 0) {
                throw new ApiException(AppConstants.INVALID_AMOUNT);
            }
            //Max amount per transaction?
            if (new BigDecimal(AppConstants.MAX_DEPOSIT_PER_TRX_AMOUNT).compareTo(transactionWrapper.getAmount()) < 0) {
                throw new ApiException(AppConstants.MAX_DEPOSIT_PER_TRX_EXCEEDED);
            }
            Account acc = this.getAccount(transactionWrapper.getAccountNumber());
            //valid Account?
            if (acc == null) {
                throw new ApiException(AppConstants.INVALID_ACC_NO); //invalid Acc
            }
            //deposits for today?
            List<Transaction> depositsToday = this.getDepositsToday(acc.getId());
            //Max deposit frequency?
            if (depositsToday.size() >= AppConstants.MAX_DEPOSIT_FREQUENCY_PER_DAY_COUNT) {
                throw new ApiException(AppConstants.MAX_DEPOSIT_FREQUENCY_PER_DAY_EXCEEDED);
            }
            BigDecimal totals = totals = depositsToday.stream().map(d -> d.getAmount()).reduce(new BigDecimal(0), BigDecimal::add);
            BigDecimal updatedTotal = totals.add(transactionWrapper.getAmount());
            BigDecimal limit = new BigDecimal(AppConstants.MAX_DEPOSIT_PER_DAY_AMOUNT);
            if (limit.compareTo(totals) < 0 || limit.compareTo(updatedTotal) < 0) {
                throw new ApiException(AppConstants.MAX_DEPOSIT_PER_DAY_EXCEEDED);
            }
            boolean hasDeposited = this.depositMoney(transactionWrapper.getAccountNumber(), transactionWrapper.getAmount());
            if (!hasDeposited) {
                throw new ApiException(AppConstants.DEPOSIT_FAILED);
            }
            wrap = this.okWrapper(this.getAccount(transactionWrapper.getAccountNumber()));
        } catch (ApiException e) {
            wrap = this.errWrapper(e.getMessage());
        }
        return wrap;
    }

    /**
     * Process withdrawal
     *
     * @param transactionWrapper
     * @return
     */
    public synchronized ResponseWrapper processWithdrawal(TransactionWrapper transactionWrapper) {
        ResponseWrapper wrap = new ResponseWrapper();
        try {
            //Amounts must be greater than 0
            if (transactionWrapper.getAmount().compareTo(new BigDecimal(1)) < 0) {
                throw new ApiException(AppConstants.INVALID_AMOUNT);
            }
            //Max amount per transaction?
            if (new BigDecimal(AppConstants.MAX_WITHDRAWAL_PER_TRX_AMOUNT).compareTo(transactionWrapper.getAmount()) < 0) {
                throw new ApiException(AppConstants.MAX_WITHDRAWAL_PER_TRX_EXCEEDED);
            }
            Account acc = this.getAccount(transactionWrapper.getAccountNumber());
            //valid Account?
            if (acc == null) {
                throw new ApiException(AppConstants.INVALID_ACC_NO); //invalid Acc
            }
            //balance available?
            if (acc.getBalance().compareTo(transactionWrapper.getAmount()) < 0) {
                throw new ApiException(AppConstants.INSUFFICIENT_BALANCE);
            }
            //withdrawals for today?
            List<Transaction> withdrawalsToday = this.getWithdrawalsToday(acc.getId());
            //Max withdrawal frequency?
            if (withdrawalsToday.size() >= AppConstants.MAX_WITHDRAWAL_FREQUENCY_PER_DAY_COUNT) {
                throw new ApiException(AppConstants.MAX_WITHDRAWAL_FREQUENCY_PER_DAY_EXCEEDED);
            }
            BigDecimal totals = withdrawalsToday.stream().map(d -> d.getAmount()).reduce(new BigDecimal(0), BigDecimal::add);
            BigDecimal updatedTotal = totals.add(transactionWrapper.getAmount());
            BigDecimal limit = new BigDecimal(AppConstants.MAX_WITHDRAWAL_PER_DAY_AMOUNT);
            if (limit.compareTo(totals) < 0 || limit.compareTo(updatedTotal) < 0) {
                throw new ApiException(AppConstants.MAX_WITHDRAWAL_PER_DAY_EXCEEDED);
            }
            boolean hasWithdrawn = this.withdrawMoney(transactionWrapper.getAccountNumber(), transactionWrapper.getAmount());
            if (!hasWithdrawn) {
                throw new ApiException(AppConstants.WITHDRAWAL_FAILED);
            }
            wrap = this.okWrapper(this.getAccount(transactionWrapper.getAccountNumber()));
        } catch (ApiException e) {
            wrap = this.errWrapper(e.getMessage());
        }
        return wrap;
    }

    /**
     * getAccount:
     *
     * @param accountNumber
     * @return account
     */
    public Account getAccount(String accountNumber) {
        return accountRepository.findByAccNumber(accountNumber);
    }

    /**
     * getAllDepositsToday
     *
     * @param accountNumber
     * @return
     */
    public List<Transaction> getDepositsToday(Long accountId) {

        return transactionRepository.findAllCreatedToday(AppConstants.TRX_CREDIT,
                accountId, this.formatQueryDate(true), this.formatQueryDate(false));
    }

    /**
     * getWithdrawalsToday
     *
     * @param accountId
     * @return
     */
    public List<Transaction> getWithdrawalsToday(Long accountId) {
        return transactionRepository.findAllCreatedToday(AppConstants.TRX_DEBIT,
                accountId, this.formatQueryDate(true), this.formatQueryDate(false));
    }

    /**
     * depositMoney
     *
     * @param accountNumber
     * @param amount
     * @return
     */
    public synchronized boolean depositMoney(String accountNumber, BigDecimal amount) {
        boolean isSuccess = false;
        try {
            Account account = this.getAccount(accountNumber);
            //record the deposit
            Transaction deposit = new Transaction(account.getId(), AppConstants.TRX_CREDIT, amount);
            deposit.setAccount(account);
            transactionRepository.save(deposit);
            //update Account balance
            BigDecimal updatedBalance = account.getBalance().add(amount);
            account.setBalance(updatedBalance);
            accountRepository.save(account);
            isSuccess = true;
        } catch (Exception e) {
            logger.error("depositMoney err: {}", e.getMessage());
        }
        return isSuccess;
    }

    /**
     * withdrawMoney
     *
     * @param accountNumber
     * @param amount
     * @return
     */
    public synchronized boolean withdrawMoney(String accountNumber, BigDecimal amount) {
        boolean isSuccess = false;
        try {
            Account account = this.getAccount(accountNumber);
            Transaction withdrawal = new Transaction(account.getId(), AppConstants.TRX_DEBIT, amount);
            withdrawal.setAccount(account);
            transactionRepository.save(withdrawal);
            //update Account balance
            BigDecimal updatedBalance = account.getBalance().subtract(amount);
            account.setBalance(updatedBalance);
            accountRepository.save(account);
            isSuccess = true;
        } catch (Exception e) {
            logger.error("withdrawMoney err: {}", e.getMessage());
        }
        return isSuccess;
    }

    /**
     * formatQueryDate for use in Query
     *
     * @param isEndDate
     * @return
     */
    public Date formatQueryDate(boolean isStartDate) {
        Date date = new Date();
        try {
            String base = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            base = base + (isStartDate ? " 00:00:00" : " 23:59:59");
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(base);
        } catch (Exception e) {
            //pass
        }
        return date;

    }

    /**
     *
     * @param errorMessage
     * @return
     */
    private ResponseWrapper errWrapper(String errorMessage) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setCode(AppConstants.BAD_CODE);
        wrap.setMessage(AppConstants.FAIL_MESSAGE);
        wrap.setError(errorMessage);
        return wrap;
    }

    /**
     *
     * @param okMessage
     * @return
     */
    private ResponseWrapper okWrapper(Object data) {
        ResponseWrapper wrap = new ResponseWrapper();
        wrap.setCode(AppConstants.OK_CODE);
        wrap.setMessage(AppConstants.OK_MESSAGE);
        wrap.setData(data);
        return wrap;
    }

}
