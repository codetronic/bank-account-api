/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.resource;

import co.tala.api.service.AccountService;
import co.tala.api.wrapper.ResponseWrapper;
import co.tala.api.wrapper.TransactionWrapper;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author aristarchus
 */
@RestController
public class AccountResource {

    @Autowired
    private AccountService accountService;

    private final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    /**
     * Get Account Balance
     *
     * @param accountNumber
     * @return
     */
    @RequestMapping(path = "/balance/{accountNumber}", method = RequestMethod.GET)
    public ResponseEntity<ResponseWrapper<Object>> getAccountBalance(
            @PathVariable(value = "accountNumber") @NotBlank @Size(min = 15) @Size(max = 15) @Valid String accountNumber) {
        logger.info("getAccountBalance params={}", accountNumber);
        ResponseWrapper wrap = this.accountService.processBalance(accountNumber);
//        logger.info("getAccountBalance result={}", wrap.getCode());
        return ResponseEntity.ok().body(wrap);
    }

    /**
     * Deposit money to account. Max deposit for the day = $150K Max deposit per
     * transaction = $40K Max deposit frequency = 4 transactions/day
     *
     * @return
     */
    @Transactional
    @RequestMapping(path = "/deposit", method = RequestMethod.POST)
    public ResponseEntity<ResponseWrapper<Object>> depositToAccount(@RequestBody TransactionWrapper transactionWrapper) {
        logger.info("depositToAccount params={}", transactionWrapper.getAccountNumber());
        ResponseWrapper wrap = this.accountService.processDeposit(transactionWrapper);
//        logger.info("depositToAccount result={}", wrap.getCode());
        return ResponseEntity.ok().body(wrap);
    }

    /**
     * Withdraw money from Account. Max withdrawal for the day = $50K Max
     * withdrawal per transaction = $20K Max withdrawal frequency = 3
     * transactions/day Cannot withdraw when balance is less than withdrawal
     * amount
     *
     * @param transactionWrapper
     * @return
     */
    @Transactional
    @RequestMapping(path = "/withdrawal", method = RequestMethod.POST)
    public ResponseEntity<ResponseWrapper<Object>> withdrawFromAccount(
            @RequestBody TransactionWrapper transactionWrapper) {
        logger.info("withdrawFromAccount params={}", transactionWrapper.getAccountNumber());
        ResponseWrapper wrap = this.accountService.processWithdrawal(transactionWrapper);
//        logger.info("withdrawFromAccount result={}", wrap.getCode());
        return ResponseEntity.ok()
                .body(wrap);
    }
}
