/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.resource;

import co.tala.api.custom.AppConstants;
import co.tala.api.wrapper.ResponseWrapper;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author aristarchus
 */
@RestController
public class ErrorResource implements ErrorController {

    @RequestMapping("/error")
    @ResponseBody
    public ResponseEntity<ResponseWrapper<Object>> handleError(HttpServletRequest request) {
        ResponseWrapper wrap = new ResponseWrapper();
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        String msg = exception.getMessage();
        wrap.setCode(statusCode);
        wrap.setMessage(AppConstants.FAIL_MESSAGE);
        wrap.setError(msg);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(wrap);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
