/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.wrapper;

import co.tala.api.custom.AppConstants;
import java.io.Serializable;

/**
 *
 * @author aristarchus
 * @param <T>
 */
public class ResponseWrapper<T extends Object> implements Serializable {

    private int code;
    private String message;
    private String error;
    private T data;
    private Long timestamp;

    public ResponseWrapper() {
        this.timestamp = System.currentTimeMillis();
        this.code = AppConstants.OK_CODE;
        this.message = AppConstants.OK_MESSAGE;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}
