/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.wrapper;

import java.math.BigDecimal;

/**
 *
 * @author aristarchus
 */
public class TransactionWrapper {

    private BigDecimal amount;
    private String accountNumber;
    private String narration;

    public TransactionWrapper() {

    }

    public TransactionWrapper(String accountNumber, BigDecimal amount, String narration) {
        this.accountNumber = accountNumber;
        this.amount = amount;
        this.narration = narration;

    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    @Override
    public String toString() {
        return String.format(
                "Transaction[account=%d, amount='%d']",
                accountNumber, amount);
    }

}
