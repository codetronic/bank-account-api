/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.repository;

import co.tala.api.entity.Account;
import co.tala.api.entity.Transaction;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author aristarchus
 */
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> findAll();

    List<Transaction> findByAccountId(Long accountId);

    Transaction findByAccount(Account account);

    @Query("select t from Transaction t where t.trx_type = :trxType and t.accountId = :accountId and t.created BETWEEN :startDate AND :endDate")
    List<Transaction> findAllCreatedToday(@Param("trxType") String trxType, @Param("accountId") Long accountId,
            @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
