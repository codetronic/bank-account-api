/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.repository;

import co.tala.api.entity.Account;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author aristarchus
 */
public interface AccountRepository extends CrudRepository<Account, Long> {

    List<Account> findAll();

    Account findByAccNumber(String accNumber);
}
