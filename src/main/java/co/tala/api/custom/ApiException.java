/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.custom;

/**
 *
 * @author aristarchus
 */
public class ApiException extends Exception {

    public ApiException(String message) {
        super(message);
    }

}
