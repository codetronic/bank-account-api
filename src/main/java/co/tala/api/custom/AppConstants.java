/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.tala.api.custom;

/**
 *
 * @author aristarchus
 */
public class AppConstants {

    public static String OK_MESSAGE = "Request Successful";
    public static String FAIL_MESSAGE = "Request Failed";
    public static int OK_CODE = 200;
    public static int BAD_CODE = 400;
    public static int ERROR_CODE = 500;

    //Validation messages
    public static String INVALID_ACC_NO = "Invalid Account Number";

    //DR CR
    public static String TRX_DEBIT = "DR";
    public static String TRX_CREDIT = "CR";

    //Deposit
    public static String MAX_DEPOSIT_PER_TRX_AMOUNT = "40000";
    public static String MAX_DEPOSIT_PER_TRX_EXCEEDED = "Exceeded Maximum Deposit Amount Per Transaction";
    public static String MAX_DEPOSIT_PER_DAY_EXCEEDED = "Exceeded Maximum Deposit Amount Per Day";
    public static String MAX_DEPOSIT_PER_DAY_AMOUNT = "150000";
    public static String MAX_DEPOSIT_FREQUENCY_PER_DAY_EXCEEDED = "Exceeded Maximum Deposit Frequency Per Day";
    public static int MAX_DEPOSIT_FREQUENCY_PER_DAY_COUNT = 4;
    public static String DEPOSIT_FAILED = "Deposit Failed";

    //Withdrawal
    public static String MAX_WITHDRAWAL_PER_TRX_AMOUNT = "20000";
    public static String MAX_WITHDRAWAL_PER_TRX_EXCEEDED = "Exceeded Maximum Withdrawal Amount Per Transaction";
    public static String MAX_WITHDRAWAL_PER_DAY_EXCEEDED = "Exceeded Maximum Withdrawal Amount Per Day";
    public static String MAX_WITHDRAWAL_PER_DAY_AMOUNT = "50000";
    public static String MAX_WITHDRAWAL_FREQUENCY_PER_DAY_EXCEEDED = "Exceeded Maximum Withdrawal Frequency Per Day";
    public static int MAX_WITHDRAWAL_FREQUENCY_PER_DAY_COUNT = 3;
    public static String INSUFFICIENT_BALANCE = "Insufficient Balance";
    public static String INVALID_AMOUNT = "Invalid Amount";
    public static String WITHDRAWAL_FAILED = "Withdrawal Failed";
}
