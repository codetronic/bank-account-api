-- Account Table
DROP TABLE IF EXISTS account;

CREATE TABLE account (
    id INT AUTO_INCREMENT  PRIMARY KEY,
    first_name VARCHAR(15) NOT NULL,
    last_name VARCHAR(15) NOT NULL,
    acc_number VARCHAR(15) NOT NULL,
    balance DECIMAL(10,2) DEFAULT 0.00,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
);

INSERT INTO account (first_name, last_name,acc_number, balance) VALUES
    ('John', 'Doe', '123456789', 43229.00);


-- Transaction Table
DROP TABLE IF EXISTS transaction;

CREATE TABLE transaction (
    id INT AUTO_INCREMENT  PRIMARY KEY,
    account INT(10) NOT NULL,
    trx_type VARCHAR(2) NOT NULL,
    amount DECIMAL(10,2) DEFAULT 0.00,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(account) REFERENCES account(id)
);

INSERT INTO transaction (account, trx_type,amount) VALUES
    (1, 'CR', 43229.00);
