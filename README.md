# Tala-Interview Backend Mini Project (Bank Account)
A simple micro web service to mimic a “Bank Account”. Through this web service, one can query about the balance, deposit money, and withdraw money.

### Development tools:
  - Java Development Kit v1.8
  - Maven build automation tool v3.3.9
  - Spring Boot Framework v2.1.6.RELEASE
  - H2 in-memory Database v1.4.199
  - Swagger UI for API documentation v2.6.1
  - Spring Boot Test bundle: JUnit 4, AssertJ, Hamcrest, Mockito, JSONassert and JsonPath
  - Jacoco v3.0 and Jacoco Maven Plugin for code coverage reports v0.7.7
  - PiTest for mutation testing v1.1.10

## Installation
This service is packaged as a JAR with Tomcat 8 embedded. It can be run using the ```java -jar bank-account-api.jar``` command.

* Clone the repository.
* Install JDK 1.8 and Maven 3.x
* You can build the project and run the tests by running ```mvn clean package```
* On build success, you can start the service by one of these two methods:
```java -jar -Dspring.profiles.active=test target/bank-account-api.jar```
or
        ```mvn spring-boot:run -Drun.arguments="spring.profiles.active=test"```
* Check the ```stdout``` or ```logs/application.log``` file to make sure no exceptions are thrown
* Once the application runs you should see something like this

```
15:12:50.610 [main] INFO  o.s.b.w.e.tomcat.TomcatWebServer - Tomcat started on port(s): 8070 (http) with context path '/api/v1'
15:12:50.612 [main] INFO  co.tala.api.Application - Started Application in 17.048 seconds (JVM running for 17.662)
```


## Project Structure

![N|swagger](https://res.cloudinary.com/ken-murage-2050/image/upload/v1564316929/tree-extended.jpg)


## Tests
Jacoco coverage reports in HTML format are stored at ```target/site/jacoco/index.html```

![N|jacoco](https://res.cloudinary.com/ken-murage-2050/image/upload/v1564317659/jacoco.jpg)

For PITest reports, run the tests using Maven, with the goal option set to: ```org.pitest:pitest-maven:mutationCoverage``` then check the reports in HTML format in the ```target/pit-reports/YYYYMMDDHHMI/index.html```

![N|pitest](https://res.cloudinary.com/ken-murage-2050/image/upload/v1564318102/pit.jpg)

## H2 Memory Database
H2 in-memory DB console can be accessed at ```http://localhost:8070/api/v1/h2-console```
with login credentials ```test/test```

![N|h2](https://res.cloudinary.com/ken-murage-2050/image/upload/v1564317477/h2.jpg)

## API Docs
API docs generated using Swagger can be accessed at ```http://localhost:8070/api/v1/swagger-ui.html```

![N|swagger](https://res.cloudinary.com/ken-murage-2050/image/upload/v1564316076/swagger.jpg)

### Todos

 - Write MORE Tests
 - Add Credit Module

License
-------------------
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.